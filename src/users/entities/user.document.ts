import { Document } from 'mongoose';
export class UserDocument extends Document {
  readonly _id: string;
  readonly name: string;
  readonly lastname: string;
  readonly dni: string;
  readonly email: string;
  readonly password?: string;
  readonly accountType: string;
  readonly rol?: string;
  readonly address?: string;
  readonly status?: string;
  readonly webSite?: string;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
