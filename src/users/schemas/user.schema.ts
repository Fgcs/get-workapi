import * as mongoose from 'mongoose';
import { UserDocument } from '../entities/user.document';

export const UserSchema = new mongoose.Schema<UserDocument>(
  {
    name: { type: String, required: true },
    lastname: { type: String, required: false, unique: false },
    dni: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    accountType: {
      type: String,
      enum: ['Professional', 'Business'],
      required: true,
    },
    rol: {
      type: String,
      enum: ['User', 'Admin'],
      required: true,
      default: 'User',
    },
    address: { type: String, required: false },
    webSite: { type: String, required: false },
    status: {
      type: String,
      enum: ['Active', 'Inactive', 'Block'],
      default: 'Active',
      required: true,
    },
  },
  { timestamps: true },
);
