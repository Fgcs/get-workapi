import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Model } from 'mongoose';
import { UserInterface } from './entities/user.interface';
@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private userModel: Model<UserInterface>) {}
  async create(createUserDto: CreateUserDto): Promise<UserInterface> {
    const createdUser = new this.userModel(createUserDto);
    return await createdUser.save().catch((err) => {
      throw new HttpException(err.keyPattern, HttpStatus.CONFLICT);
    });
  }

  async findAll(): Promise<UserInterface[]> {
    return await this.userModel.find().exec();
  }
  async getByEmail(email: string): Promise<UserInterface> {
    return await this.userModel
      .findOne({
        $or: [{ email: email.toLowerCase() }],
      })
      .exec()
      .catch((err) => {
        throw new HttpException(
          'User with this email does not exist',
          HttpStatus.NOT_FOUND,
        );
      });
  }
  async findOne(id: string): Promise<UserInterface> {
    return await this.userModel
      .findOne({ _id: id })
      .exec()
      .catch((err) => {
        throw new HttpException(
          'User with this id does not id',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  async update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserInterface> {
    return await this.userModel
      .findOneAndUpdate({ _id: id }, updateUserDto)
      .exec();
  }

  async remove(id: string): Promise<UserInterface> {
    return await this.userModel.findOneAndDelete({ _id: id }).exec();
  }
}
