import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerCustomOptions,
} from '@nestjs/swagger';
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors();
  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('getwork-Api')
    .setDescription('get work API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle: 'get work API',
  };
  SwaggerModule.setup('api', app, document, customOptions);
  await app.listen(3000);
}
bootstrap();
