import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { configDB } from './shared/database-config';
import { PostsModule } from './posts/posts.module';
@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${configDB.url}/${configDB.name}`, {
      useNewUrlParser: true,
    }),
    AuthModule,
    UsersModule,
    PostsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
