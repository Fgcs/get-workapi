import * as mongoose from 'mongoose';
import { Auth } from '../entities/auth.entity';

export const AuthSchema = new mongoose.Schema<Auth>({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  dateLogin: { type: Date, required: true },
  tokenType: { type: String, required: true },
  token: { type: String, required: true },
  expiresIn: { type: String, required: true },
});
