import { Document } from 'mongoose';
import { UserInterface } from 'src/users/entities/user.interface';
export class Auth extends Document {
  readonly _id?: string;
  readonly dateLogin: Date;
  readonly tokenType: string;
  readonly token: string;
  readonly expiresIn: string;
  readonly user?: UserInterface;
}
