import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { compareSync, hashSync } from 'bcryptjs';
import { RegisterDto } from './dto/register.dto';
import { AuthLoginDto } from './dto/auth-login.dto';
import { Auth } from './entities/auth.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class AuthService {
  constructor(
    @InjectModel('Auth') private readonly AuthDB: Model<Auth>,
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async register(user: RegisterDto) {
    user.email = user.email.toLowerCase();
    user.lastname = user.lastname.toLowerCase();
    user.password = hashSync(user.password, 10);
    try {
      const createdUser = await this.usersService.create({
        ...user,
      });
      createdUser.password = undefined;
      return createdUser;
    } catch (error) {
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async login(auth: AuthLoginDto): Promise<Auth> {
    const { email, password } = auth;
    const userDB = await this.usersService.getByEmail(email);
    if (!userDB) throw new NotFoundException('User not found');
    const matchPassword = compareSync(password, userDB.password);

    if (!matchPassword) throw new UnauthorizedException('Password Incorrect');
    if (userDB.status === 'inactive')
      throw new UnauthorizedException('User Inactive');
    const payload = {
      _id: String(userDB._id),
      email: userDB.email,
    };
    userDB.password = undefined;
    const expiresTime = 24 * 60 * 60;
    const newAuth = new this.AuthDB({
      dateLogin: Date(),
      tokenType: 'Bearer',
      token: this.jwtService.sign(payload, { expiresIn: '1d' }),
      expiresIn: expiresTime,
      user: userDB,
    });
    return await newAuth.save();
  }
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}
