export const configDB = {
  url:
    process.env.NODE_ENV === 'development'
      ? 'localhost:27017'
      : 'localhost:27017',
  name: 'getWork-api',
};
