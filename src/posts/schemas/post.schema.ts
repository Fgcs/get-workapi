import * as mongoose from 'mongoose';
import { UserDocument } from 'src/users/entities/user.document';
import { PostDocument } from '../entities/post.document';

export const PostSchema = new mongoose.Schema<PostDocument>(
  {
    title: { type: String, required: true },
    description: { type: String, required: false },
    position: { type: String, required: true },
    teme: { type: String, required: true },
    company: { type: String, required: true },
    typeOfWorkplace: {
      type: String,
      enum: ['remote', 'face-to-face'],
      default: 'remote',
    },
    typeJob: {
      type: String,
      enum: ['full time', 'part time'],
      default: 'full time',
    },
    address: { type: String, required: false },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    status: {
      type: String,
      enum: ['Active', 'Inactive', 'Block'],
      default: 'Active',
      required: true,
    },
  },
  { timestamps: true },
);
