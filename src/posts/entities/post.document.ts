import { Document } from 'mongoose';
import { UserInterface } from 'src/users/entities/user.interface';
export class PostDocument extends Document {
  readonly _id: string;
  readonly title: string;
  readonly description: string;
  readonly position: string;
  readonly company: string;
  readonly typeOfWorkplace: string;
  readonly teme: string;
  readonly typeJob: string;
  readonly address: string;
  readonly status: string;
  readonly author: UserInterface;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
