import { UserInterface } from 'src/users/entities/user.interface';

export class PostInterface {
  _id: string;
  title: string;
  description: string;
  position: string;
  company: string;
  typeOfWorkplace: string; //
  teme: string;
  typeJob: string; //
  address?: string;
  status?: string;
  author: UserInterface;
  createdAt?: Date;
  updatedAt?: Date;
}
