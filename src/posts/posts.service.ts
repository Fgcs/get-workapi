import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { PostInterface } from './entities/post.entity';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UsersService } from 'src/users/users.service';
@Injectable()
export class PostsService {
  constructor(
    @InjectModel('Post') private PostDb: Model<PostInterface>,
    private readonly usersService: UsersService,
  ) {}
  async create(createPostDto: CreatePostDto): Promise<PostInterface> {
    const user = await this.usersService.findOne(createPostDto.userId);
    if (!user) throw new HttpException('user no foubd', HttpStatus.CONFLICT);
    const createdPost = new this.PostDb(createPostDto);
    createdPost.author = user;
    return await createdPost.save().catch((err) => {
      throw new HttpException(err.keyPattern, HttpStatus.CONFLICT);
    });
  }

  async findAll(): Promise<PostInterface[]> {
    return await this.PostDb.find()
      .populate({
        path: 'author',
        select: ['_id', 'name', 'lastname'],
      })
      .exec();
  }

  async findOne(id: string): Promise<PostInterface> {
    return await this.PostDb.findOne({ _id: id })
      .populate({
        path: 'author',
        select: ['_id', 'name', 'lastname'],
      })
      .exec()
      .catch((err) => {
        throw new HttpException(
          'Post with this id does not id',
          HttpStatus.NOT_FOUND,
        );
      });
  }

  async update(
    id: string,
    updatePostDto: UpdatePostDto,
  ): Promise<PostInterface> {
    return await this.PostDb.findOneAndUpdate(
      { _id: id },
      updatePostDto,
    ).exec();
  }

  async remove(id: string): Promise<PostInterface> {
    return await this.PostDb.findOneAndDelete({ _id: id }).exec();
  }
  async getByAuthor(id: string): Promise<PostInterface[]> {
    return await this.PostDb.find({ author: id }).catch((err) => {
      throw new HttpException('Post with this user id  ', HttpStatus.NOT_FOUND);
    });
  }
}
